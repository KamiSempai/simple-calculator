package ru.kamisempai.simplecalculator.model;



public class CalculatorModel {
	public static final char SYMBOL_POINT = '.';
	public static final int OPERATION_NONE = 0;
	
	public enum State {
		ENTER_NUMBER,
		ENTER_OPERATION,
		SHOW_RESULT,
		ERROR
	}
	
	private static final String ERROR_MESSAGE = "ERROR!";
	
	private State mState = State.ENTER_NUMBER;
	private StringBuilder mValue1StringBuilder;
	private double mValue2;
	private boolean isHavePoint;
	
	private TwoValueOperation mCurrentOperation;
	private OnValueChangeListener mOnValueChangeListener;
	
	public CalculatorModel() {
		mValue1StringBuilder = new StringBuilder();
	}

	public void putSymbol(char symbol) {
		if(mState != State.ERROR) {
			setState(State.ENTER_NUMBER);
			
			if(symbol == SYMBOL_POINT) {
				if(!isHavePoint) {
					if(mValue1StringBuilder.length() == 0)
						mValue1StringBuilder.append('0');
					isHavePoint = true;
					mValue1StringBuilder.append(symbol);
					notifyValueChanged();
				}
			}
			else {
				mValue1StringBuilder.append(symbol);
				notifyValueChanged();
			}
		}
	}

	public void clear() {
		mValue2 = 0;
		mValue1StringBuilder.delete(0, mValue1StringBuilder.length());
		isHavePoint = false;
		mCurrentOperation = null;
		setState(State.ENTER_NUMBER);
		notifyValueChanged();
	}

	public void calc() {
		if(mCurrentOperation != null && mState == State.ENTER_NUMBER) {
			try {
				mValue2 = mCurrentOperation.calc(mValue2, getFirstValue());
				mCurrentOperation = null;
				setState(State.SHOW_RESULT);
			} catch (Exception exception) {
				handleError(exception);
			}
			notifyValueChanged();
		}
	}

	public void handleOperation(SingleValueOperation operation) {
		if(mState != State.ERROR) {
			if(mState == State.ENTER_NUMBER) {
				double value = getFirstValue();
				try {
					mValue2 = operation.calc(value);
				} catch (Exception exception) {
					handleError(exception);
				}
			}
			else {
				try {
					mValue2 = operation.calc(mValue2);
				} catch (Exception exception) {
					handleError(exception);
				}
			}
			notifyValueChanged();
		}
	}

	public void handleOperation(TwoValueOperation operation) {
		if(mState != State.ERROR) {
			if(mState == State.ENTER_NUMBER) {
				if(mCurrentOperation != null) calc();
				else mValue2 = getFirstValue();
			}
			mCurrentOperation = operation;
			setState(State.ENTER_OPERATION);
		}
	}
	
	public void handleOperation(NoValueOperation operation) {
		try {
			operation.calc(this);
		} catch (Exception exception) {
			handleError(exception);
		}
	}

	public String getDisplayValue() {
		switch (mState) {
		case ENTER_NUMBER:
		case ENTER_OPERATION:
			if(mValue1StringBuilder.length() == 0) return "0";
			return mValue1StringBuilder.toString();
		case SHOW_RESULT:
			return format(mValue2);
		case ERROR:
			return ERROR_MESSAGE;
		}
		return "";
	}

	public void setOnValueChangeListener(OnValueChangeListener listener) {
		mOnValueChangeListener = listener;
	}

	public OnValueChangeListener getOnValueChangeListener() {
		return mOnValueChangeListener;
	}

// =============================================================
// Public interfaces
// =============================================================
	
	public interface OnValueChangeListener {
		public void onValueChange(CalculatorModel model);
	}

	public interface NoValueOperation {
		public void calc(CalculatorModel model) throws Exception;
	}

	public interface SingleValueOperation {
		public double calc(double value) throws Exception;
	}
	
	public interface TwoValueOperation {
		public double calc(double value1, double value2) throws Exception;
	}
	
// =============================================================
// Private Classes and methods
// =============================================================
	
	private double getFirstValue() {
		if (mValue1StringBuilder.length() == 0
				|| mValue1StringBuilder.charAt(mValue1StringBuilder.length() - 1) == SYMBOL_POINT) {
			mValue1StringBuilder.append('0');
		}
		return Double.parseDouble(mValue1StringBuilder.toString());
	}
	
	private void setState(State state) {
		switch (state) {
		case ENTER_NUMBER:
			if(mState != State.ENTER_NUMBER) {
				mValue1StringBuilder.delete(0, mValue1StringBuilder.length());
				isHavePoint = false;
				mState = State.ENTER_NUMBER;
				notifyStateChanged();
			}
			break;
		default:
			if(mState != state) {
				mState = state;
				notifyStateChanged();
			}
			break;
		}
	}
	
	private void handleError(Exception exception) {
		setState(State.ERROR);
		notifyValueChanged();
	}
	
	private void notifyValueChanged() {
		if(mOnValueChangeListener != null)
			mOnValueChangeListener.onValueChange(this);
	}
	
	private void notifyStateChanged() {
		
	}
	
	private String format(double value)  {
		if(value == (int) value)
			return Integer.toString((int) value);
		return Double.toString(value);
	}

}

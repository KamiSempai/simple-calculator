package ru.kamisempai.simplecalculator.model.operations;

import ru.kamisempai.simplecalculator.model.CalculatorModel.TwoValueOperation;

public class OperationPlus implements TwoValueOperation {

	@Override
	public double calc(double value1, double value2) {
		return value1 + value2;
	}

}

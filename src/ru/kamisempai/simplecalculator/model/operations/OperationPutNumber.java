package ru.kamisempai.simplecalculator.model.operations;

import ru.kamisempai.simplecalculator.model.CalculatorModel;
import ru.kamisempai.simplecalculator.model.CalculatorModel.NoValueOperation;

public class OperationPutNumber implements NoValueOperation{
	private final char mNumber;
	
	public OperationPutNumber(char number) {
		mNumber = number;
	}

	@Override
	public void calc(CalculatorModel model) {
		model.putSymbol(mNumber);
	}

}
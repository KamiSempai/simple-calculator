package ru.kamisempai.simplecalculator.model.operations;

import ru.kamisempai.simplecalculator.model.CalculatorModel.SingleValueOperation;

public class OperationCosinus implements SingleValueOperation{
	
	public static final OperationCosinus INSTANCE = new OperationCosinus();

	@Override
	public double calc(double value) {
		return Math.cos(value);
	}

}

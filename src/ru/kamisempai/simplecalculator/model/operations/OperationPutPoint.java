package ru.kamisempai.simplecalculator.model.operations;

import ru.kamisempai.simplecalculator.model.CalculatorModel;
import ru.kamisempai.simplecalculator.model.CalculatorModel.NoValueOperation;

public class OperationPutPoint implements NoValueOperation{
	
	@Override
	public void calc(CalculatorModel model) {
		model.putSymbol(CalculatorModel.SYMBOL_POINT);
	}

}

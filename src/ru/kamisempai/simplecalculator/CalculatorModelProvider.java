package ru.kamisempai.simplecalculator;

import ru.kamisempai.simplecalculator.model.CalculatorModel;

public interface CalculatorModelProvider {
	public CalculatorModel getCalculatorModel();
}

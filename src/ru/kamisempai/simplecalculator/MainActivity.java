package ru.kamisempai.simplecalculator;

import ru.kamisempai.simplecalculator.model.CalculatorModel;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

public class MainActivity extends FragmentActivity implements CalculatorModelProvider{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public CalculatorModel getCalculatorModel() {
		return CalculatorApplication.getModel();
	}
	
}

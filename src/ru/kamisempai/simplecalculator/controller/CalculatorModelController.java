package ru.kamisempai.simplecalculator.controller;

import ru.kamisempai.simplecalculator.model.CalculatorModel;
import ru.kamisempai.simplecalculator.model.CalculatorModel.NoValueOperation;
import ru.kamisempai.simplecalculator.model.CalculatorModel.SingleValueOperation;
import ru.kamisempai.simplecalculator.model.CalculatorModel.TwoValueOperation;
import android.util.SparseArray;

public class CalculatorModelController {
	
	private CalculatorModel mModel;
	private SparseArray<Object> mOperations;
	
	public CalculatorModelController(CalculatorModel model) {
		mModel = model;
		mOperations = new SparseArray<Object>();
	}

	public void addOperation(int id, SingleValueOperation operation) {
		mOperations.put(id, operation);
	}

	public void addOperation(int id, TwoValueOperation operation) {
		mOperations.put(id, operation);
	}
	
	public void addOperation(int id, NoValueOperation operation){
		mOperations.put(id, operation);
	}

	public void deleteOperation(int id) {
		mOperations.remove(id);
	}

	public void deleteAllOperations() {
		mOperations.clear();
	}

	public void handleOperation(int operation) {
		Object operationObject = mOperations.get(operation);
		if(operationObject != null) {
			if(operationObject instanceof NoValueOperation)
				mModel.handleOperation((NoValueOperation) operationObject);
			if(operationObject instanceof SingleValueOperation)
				mModel.handleOperation((SingleValueOperation) operationObject);
			if(operationObject instanceof TwoValueOperation)
				mModel.handleOperation((TwoValueOperation) operationObject);
		}
		else
			throw new UnsupportedOperationException("Operation ID = " + operation);
	}
}

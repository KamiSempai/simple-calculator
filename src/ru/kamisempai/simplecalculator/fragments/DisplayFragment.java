package ru.kamisempai.simplecalculator.fragments;

import ru.kamisempai.simplecalculator.CalculatorModelProvider;
import ru.kamisempai.simplecalculator.R;
import ru.kamisempai.simplecalculator.model.CalculatorModel;
import ru.kamisempai.simplecalculator.model.CalculatorModel.OnValueChangeListener;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DisplayFragment extends Fragment {
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if(!(activity instanceof CalculatorModelProvider))
			throw new RuntimeException("Activity must implement CalculatorModelProvider.");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_display, null);
		final TextView displayText = (TextView) rootView.findViewById(R.id.txtResult);
		
		CalculatorModel model = ((CalculatorModelProvider) getActivity()).getCalculatorModel();
		OnValueChangeListener listener = new OnValueChangeListener() {
			@Override
			public void onValueChange(CalculatorModel model) {
				displayText.setText(model.getDisplayValue());
			}
		};
		listener.onValueChange(model);
		model.setOnValueChangeListener(listener);
		
		return rootView;
	}
}

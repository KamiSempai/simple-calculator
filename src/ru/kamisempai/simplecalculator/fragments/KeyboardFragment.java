package ru.kamisempai.simplecalculator.fragments;

import ru.kamisempai.simplecalculator.CalculatorModelProvider;
import ru.kamisempai.simplecalculator.R;
import ru.kamisempai.simplecalculator.controller.CalculatorModelController;
import ru.kamisempai.simplecalculator.model.operations.OperationClear;
import ru.kamisempai.simplecalculator.model.operations.OperationCosinus;
import ru.kamisempai.simplecalculator.model.operations.OperationDivision;
import ru.kamisempai.simplecalculator.model.operations.OperationEqual;
import ru.kamisempai.simplecalculator.model.operations.OperationMinus;
import ru.kamisempai.simplecalculator.model.operations.OperationMultiplication;
import ru.kamisempai.simplecalculator.model.operations.OperationPlus;
import ru.kamisempai.simplecalculator.model.operations.OperationPutNumber;
import ru.kamisempai.simplecalculator.model.operations.OperationPutPoint;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class KeyboardFragment extends Fragment implements OnClickListener {
	private CalculatorModelController mModelController;
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		if(!(activity instanceof CalculatorModelProvider))
			throw new RuntimeException("Activity must implement CalculatorModelProvider.");
		mModelController = new CalculatorModelController(((CalculatorModelProvider) activity).getCalculatorModel());
		mModelController.addOperation(R.id.btn0, new OperationPutNumber('0'));
		mModelController.addOperation(R.id.btn1, new OperationPutNumber('1'));
		mModelController.addOperation(R.id.btn2, new OperationPutNumber('2'));
		mModelController.addOperation(R.id.btn3, new OperationPutNumber('3'));
		mModelController.addOperation(R.id.btn4, new OperationPutNumber('4'));
		mModelController.addOperation(R.id.btn5, new OperationPutNumber('5'));
		mModelController.addOperation(R.id.btn6, new OperationPutNumber('6'));
		mModelController.addOperation(R.id.btn7, new OperationPutNumber('7'));
		mModelController.addOperation(R.id.btn8, new OperationPutNumber('8'));
		mModelController.addOperation(R.id.btn9, new OperationPutNumber('9'));
		
		mModelController.addOperation(R.id.btnClear, new OperationClear());
		mModelController.addOperation(R.id.btnPoint, new OperationPutPoint());
		mModelController.addOperation(R.id.btnEquall, new OperationEqual());

		mModelController.addOperation(R.id.btnPlus, new OperationPlus());
		mModelController.addOperation(R.id.btnMinus, new OperationMinus());
		mModelController.addOperation(R.id.btnMult, new OperationMultiplication());
		mModelController.addOperation(R.id.btnDiv, new OperationDivision());
		
		mModelController.addOperation(R.id.btnCos, new OperationCosinus());
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_keyboard, null);
		findButtonsAndSetOnClickListener(rootView);
		return rootView;
	}

	@Override
	public void onClick(View v) {
		mModelController.handleOperation(v.getId());
	}
	
	private void findButtonsAndSetOnClickListener(ViewGroup viewGroup) {
		for(int i = 0; i < viewGroup.getChildCount(); i++) {
			View child = viewGroup.getChildAt(i);
			if(child instanceof Button)
				child.setOnClickListener(this);
			else if(child instanceof ViewGroup)
				findButtonsAndSetOnClickListener((ViewGroup) child);
		}
	}
}

package ru.kamisempai.simplecalculator;

import ru.kamisempai.simplecalculator.model.CalculatorModel;
import android.app.Application;

public class CalculatorApplication extends Application {

	private static CalculatorModel mModel;
	public static synchronized CalculatorModel getModel() {
		if(mModel == null) mModel = new CalculatorModel();
		return mModel;
	}
}
